/* SILAR - SImple Little ARm. v1.5 (c) 2019 Nathan Cermak, CC-BY-NC license.
*  revision 2019-04-11: 
*     reduced thickness of adaptor, arm1, and arm2 from 10 or 8 mm to 7 or 5 mm
*     increased width of servo hole on baseplate
*     made baseplate taller
*     shortened arm1     
*/

include <servo_arm.scad>;

fn=64;

module spline(ID, OD){
    x = 1;
}
    

module servo(){
    w = 12.2;
    l = 23;
    h=24.4;
    holeSpacing = 28.4;
    l2 = 31.2;
    translate([0,0,h]) cylinder(h=6.7, d=6, $fn=fn);
   translate([-l/2, -w/2,0]) {  // not accurate but fine for build purposes
        cube([l, w,h]);
        difference(){
            translate([-(l2-l)/2, 0, 18.8]) cube([l2,w, 2.5]);
            translate([ (l-holeSpacing)/2,w/2,0]) cylinder(h=1000,d=3, $fn=fn, center=true); 
            translate([ (l+holeSpacing)/2,w/2,0]) cylinder(h=1000,d=3, $fn=fn, center=true);    
        }
    }
}
//translate([0,0,3]) servo();


module baseplate(){
    color("blue") 
    difference(){ 
        union(){
            translate([-32.5,-32.5,0]) cube([65,65,5]);
            translate([-19,-15,0]) cube([12,30,22]);
            translate([7,  -15,0]) cube([12,30,22]);
        }

        translate([0,0,4]) servo();
        
        translate([-20,-6.4,-1]) cube([40,12.8,18]); // hole for wires to exit
        
        //vertical holes for bolting down servo
        translate([-14.2, 0 ,-0.01]) cylinder(d=3, h=100, $fn=fn, center=true);
        translate([-14.2+28.4,0,-0.01]) cylinder(d=3, h=100, $fn=fn, center=true);
        
        //M6 / 1/4"-20 holes for bolting baseplate to table
        for (i = [-25.4:25.4:76.2]){
            for (j = [-25.4:25.4:25.4]){
                translate([i,j,-1]) cylinder(100,d=7, $fn=fn);
            }
        }
    }
}
//baseplate();

module adaptor1(){
    color("green")
    difference(){
        translate([-18,-3.5,0]) cube([36,7,18]);

        // subtract out space for servo to sit
        translate([-11.5,-50,6]) cube([23,100,100]);

        // holes to bolt servo onto adaptor (notap for M2) 
        translate([-14.2,0,12]) rotate([90,0,0]) cylinder(d=2.5, h=1000, center=true, $fn=fn);
        translate([14.2,0,12]) rotate([90,0,0]) cylinder(d=2.5, h=1000, center=true, $fn=fn);

        // countersunk holes for attaching to horn
        translate([-8.5, 0,-0.01]) cylinder(d=2.5, h=100, $fn=fn); 
        translate([-8.5, 0,4]) cylinder(d=5.5, h=100, $fn=fn); 
        translate([8.5,0,-0.01]) cylinder(d=2.5, h=100, $fn=fn);
        translate([8.5, 0,4]) cylinder(d=5.5, h=100, $fn=fn); 
        cylinder(d=5.5,h=2, $fn=fn, center=true);
    }
}
//adaptor1(); 


module adaptor1_spline(){
    color("green")
    difference(){
        translate([-18,-3.5,-1]) cube([36,7,19]);

        // subtract out space for servo to sit
        translate([-11.5,-50,6]) cube([23,100,100]);

        // holes to bolt servo onto adaptor (notap for M2) 
        translate([-14.2,0,12]) rotate([90,0,0]) cylinder(d=2.5, h=1000, center=true, $fn=fn);
        translate([14.2,0,12]) rotate([90,0,0]) cylinder(d=2.5, h=1000, center=true, $fn=fn);

        // countersunk holes for attaching to horn 
        // 6.25mm lateral shift to account for offset between rotational axis and center of servo
        translate([6.25, 0, 5+8.5]) cylinder(d=2.2, h=100, $fn=fn,center=true);
        translate([6.25, 0, -1.01]) servo_head();
        translate([6.25, 0, 4.5]) cylinder(d=5, h=100, $fn=fn);
    }
}
//adaptor1_spline(); 

/*module arm1(){
   // color("purple")
    difference(){
        translate([-3.5,-2.5,0]) cube([7,5,arm1Length+27]);
        
        // holes to attach to horns 
        translate([0,50,5]) rotate([90,0,0]) cylinder(d=2.5, h=1000, $fn=fn);
        translate([0,3.6,5+8.5]) rotate([90,0,0]) cylinder(d=5.5, h=2, $fn=fn);
        translate([0,50,5+17]) rotate([90,0,0]) cylinder(d=2.5, h=1000, $fn=fn);
        translate([0,50,5+arm1Length]) rotate([90,0,0]) cylinder(d=2.5, h=1000, $fn=fn);
        translate([0,3.6,5+arm1Length+8.5]) rotate([90,0,0]) cylinder(d=5.5, h=2, $fn=fn);
        translate([0,50,5+arm1Length+17]) rotate([90,0,0]) cylinder(d=2.5, h=1000, $fn=fn);
    }
}*/
//arm1();

module arm1_spline(){
    color("purple")
    difference(){
        translate([-3.5,0,0]) cube([7,5,arm1Length+10]);
        
        // servo spline holes 
        translate([0, 4, 5]) rotate([90,0,0]) cylinder(d=2.2, h=100, $fn=fn,center=true);
        translate([0, -0.01, 5]) rotate([-90,0,0]) servo_head();
        translate([0, 4, arm1Length+5]) rotate([90,0,0]) cylinder(d=2.2, h=100, $fn=fn,center=true);
        translate([0, -0.01, arm1Length+5]) rotate([-90,0,0]) servo_head();
    }
}
arm1_spline();

module arm2(){
    color("green")
    union(){
        
        difference(){
            translate([-18,-3.5,0]) cube([36,7,18]);
            // subtract out space for servo to sit
            translate([-11.5,-50,6]) cube([23,100,100]);
            // holes to bolt onto cylinder (tap for M3) 
            translate([-14.2,0,12]) rotate([90,0,0]) cylinder(d=2.5, h=1000, center=true, $fn=fn);
            translate([14.2,0,12]) rotate([90,0,0]) cylinder(d=2.5, h=1000, center=true, $fn=fn);
        }
        
        translate([17.9,0,12]){
            difference(){
                translate([arm2Length/2,0,0]) cube([arm2Length,7,8], center=true);   
                // holes in arm for screw mounts (M2.5)
                for (j = [5:6:(arm2Length-3)]){
                    translate([j,0,0]) rotate([90,0,0])  cylinder(h=100, d=2.3, center=true, $fn=fn);
                    translate([j,0,0]) cylinder(h=100, d=2.3, center=true, $fn=fn);
                }
                translate([arm2Length-20,0,0]) rotate([0,90,0]) cylinder(h=100, d=2.3, $fn=fn);
            }
        }
    }
}
//arm2();


arm1Length = 45;
arm2Length = 40;

//baseplate();
//translate([0,0,43])      adaptor1();
//translate([-10,-31,45])  arm1();
//translate([0,0,43+arm1Length+47])  rotate([0,180,0])    arm2();

/*
fitecServo();
translate([-10.5,20,66]) rotate([90,0,0]) servo();
translate([-10.5,20,66+arm1Length]) rotate([90,0,0]) servo();
*/


