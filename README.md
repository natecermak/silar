# Simple Little Arm (SILAR)

SILAR is a very simple 3D printed and submicro servomotor based robotic arm. Its arms are 45 and 65 mm long, meaning it's good for movements on scales of millimeters to up to 10ish centerimeters. 

## Benchmarking
### Speed (TBD)

### Reproducibility (TBD)

### Range of motion (TBD)

### Kinematic equations (TBD)

## Building SILAR (TBD)

 - print on a high resolution printer. I used an Objet30 Pro printer. The high resolution is specifically necessary for the spline teeth the arm1_spline.stl and adaptor_spline.stl files. 
 - Before beginning to build, set all servos to 90 degrees
 - Assemble from the base up. Assemble with arm1 vertical and arm2 horizontal. 
 
## File descriptions
 
  - *SILAR.scad* OpenSCAD design file containing all the individual parts. 
  - *silar_bom.xlsx* SILAR Bill of Materials (what you need to build SILAR and where to buy the stuff)
  - *adaptor1.stl* and *adaptor1_spline.stl*
  - *arm1.stl* and *arm1_spline.stl*
  - *arm2.stl*
  - *baseplate.stl*